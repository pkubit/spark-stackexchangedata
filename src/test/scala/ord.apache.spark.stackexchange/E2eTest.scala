package test.scala.ord.apache.spark.stackexchange

import org.apache.spark.sql.SparkSession
import org.apache.spark.stackexchange.Application.loadProperties
import org.apache.spark.stackexchange.data.Tag
import org.apache.spark.stackexchange.spark.TagDataJob
import org.junit.runner.RunWith
import org.scalatest.{FlatSpec, Matchers}
import org.scalatest.junit.JUnitRunner


@RunWith(classOf[JUnitRunner])
class E2eTest extends FlatSpec with Matchers {

  val sparkSession = SparkSession.builder
    .appName("Simple Application")
    .config("spark.master", "local")
    .getOrCreate()

  val configFilePath: String = "src/test/resources/config.properties"
  val properties = loadProperties(configFilePath)

  behavior of "TagDataJob"

  it should "produce the correct output" in {

    //TODO Make use of spark-testing-base and compare parallelized datasets instead of collected Array
    val expectedOutput = Array(
      Tag(97, "app6", 6, Some(28207), Some(28206)),
      Tag(97, "app6", 5, Some(28207), Some(28206)),
      Tag(97, "app6", 4, Some(28207), Some(28206))
    )

    val outputDataSet = TagDataJob.runPipeline(sparkSession, properties)
    val outputString = outputDataSet.collect()

    assert(outputString(0) == expectedOutput(0) )
    assert(outputString(1) == expectedOutput(1) )
    assert(outputString(2) == expectedOutput(2) )

  }
}


