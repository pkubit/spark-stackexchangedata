package org.apache.spark.stackexchange.xml

import java.io.StringReader
import javax.xml.parsers.SAXParserFactory

import org.xml.sax.helpers.DefaultHandler
import org.xml.sax.{Attributes, InputSource}

import scala.util.Try


object XmlDataParser {

  private val SAX_PARSER_INSTANCE = SAXParserFactory.newInstance().newSAXParser()

  def parseXmlRow(row: String): Try[Map[String, String]] = {
    Try {
      val inputSource = new InputSource(new StringReader(row))
      val fieldsMap = scala.collection.mutable.Map[String, String]()
      val defaultHandler = createHandler(fieldsMap)
      SAX_PARSER_INSTANCE.parse(inputSource, defaultHandler)
      //make return value immutable
      fieldsMap.toMap
    }
  }

  private def createHandler(fieldsMap: scala.collection.mutable.Map[String, String]): DefaultHandler = {
    new DefaultHandler {
      override def startElement(uri: String, localName: String, qName: String, attributes: Attributes): Unit = {
        require(fieldsMap.isEmpty)
        for (x <- 0 to attributes.getLength) {
          fieldsMap.put(attributes.getQName(x), attributes.getValue(x))
        }
      }
    }
  }

}
