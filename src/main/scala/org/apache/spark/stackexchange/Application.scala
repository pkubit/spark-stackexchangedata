package org.apache.spark.stackexchange

import java.io.FileInputStream
import java.util.Properties

import org.apache.spark.sql.SparkSession
import org.apache.spark.stackexchange.spark.{TagDataJob, UsersDataJob}
import org.apache.spark.stackexchange.xml.XmlDataParser

import scala.util.Try


object Application {

  def main(args: Array[String]): Unit = {

    val configFilePath: String = Try(args(0)).getOrElse("src/main/resources/config.properties")
    val properties = loadProperties(configFilePath)

    val sparkSession = SparkSession.builder
      .appName("Simple Application")
      .getOrCreate()

    processData(sparkSession, properties)

  }

  def processData(sparkSession: SparkSession, properties: Properties) = {
    val tagsResult = TagDataJob.runPipeline(sparkSession, properties)
    TagDataJob.getSinkFunction(properties).createSink(tagsResult)

    val usersResult = UsersDataJob.runPipeline(sparkSession, properties)
    UsersDataJob.getSinkFunction(properties).createSink(usersResult)

  }


  def loadProperties(configFilePath: String): Properties = {
    val configFile = new Properties()
    configFile.load(new FileInputStream(configFilePath))
    configFile
  }

}
