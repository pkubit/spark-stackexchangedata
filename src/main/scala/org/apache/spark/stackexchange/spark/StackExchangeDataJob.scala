package org.apache.spark.stackexchange.spark

import org.apache.log4j.LogManager
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Dataset, SparkSession}
import org.apache.spark.stackexchange.xml.XmlDataParser

object StackExchangeDataJob {

  val logger = LogManager.getRootLogger
  val DATA_FILE_CONFIG_VALUE = "data.file.path"
  val OUTPUT_FILE_CONFIG_VALUE = "output.file.path"
  val RECORDS_LIMIT_CONFIG_VALUE = "output.records.limit"

  def getInputDataFromFile(sparkSession: SparkSession, pathToDataFile: String): Dataset[String] = {
    import sparkSession.implicits._
    logger.info("Will now read data from file")
    sparkSession.read.textFile(pathToDataFile).as[String]
  }

  def parseInputXmlData(sparkSession: SparkSession, inputData: Dataset[String]): RDD[Map[String, String]] = {
    logger.info("Will now start to parse data from xml.")
    import sparkSession.implicits._
    inputData
      .map(row => row.trim)
      .filter(row => row.startsWith("<row")).rdd
      .map(XmlDataParser.parseXmlRow)
      .flatMap(_.toOption)
  }

}
