package org.apache.spark.stackexchange.spark

import java.util.Properties

import main.scala.org.apache.spark.stackexchange.data.parsing.TagParser
import org.apache.log4j.LogManager
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.spark.stackexchange.commons.CaseImplicit.tryCase
import org.apache.spark.stackexchange.data.Tag
import org.apache.spark.stackexchange.io.{FileSinkFromDataset, SinkFunction}
import org.apache.spark.stackexchange.spark.StackExchangeDataJob._

object TagDataJob {

  val logger = LogManager.getRootLogger
  val JOB_PREFIX = "tags."



  def runPipeline(sparkSession: SparkSession, properties: Properties) = {
    logger.info("TagDataJob start")
    val recordsLimit = properties.getProperty(JOB_PREFIX + RECORDS_LIMIT_CONFIG_VALUE).toInt
    logger.info("Data read success. Start processing data")
    val inputData = getInputData(sparkSession, properties)
    val xmlRDD = parseInputXmlData(sparkSession, inputData)
    val tagRDD = parseTagData(xmlRDD)
    val tagDataset = toDataset(sparkSession, tagRDD)
    logger.info(s"Data processed. Will now write $recordsLimit records to sink")
    sortAndLimit(sparkSession, tagDataset, recordsLimit)
  }

  def getInputData(session: SparkSession, properties: Properties): Dataset[String] = {
    val pathToDataFile = properties.getProperty(JOB_PREFIX + DATA_FILE_CONFIG_VALUE)
    getInputDataFromFile(session: SparkSession, pathToDataFile)
  }

  def parseTagData(xmlRdd: RDD[Map[String, String]]): RDD[Tag] = {
    logger.info("Will now parse raw data to Tags")
    xmlRdd.map(TagParser.xmlToTagData)
      .map(tryCase[Tag])
      .filter(_.isSuccess)
      .map(_.get)
  }

  def getSinkFunction(properties: Properties): SinkFunction[Dataset[Tag]] = {
    new FileSinkFromDataset[Tag](properties.getProperty(JOB_PREFIX + OUTPUT_FILE_CONFIG_VALUE))
  }

  def sortAndLimit(sparkSession: SparkSession, tagDataset: Dataset[Tag], recordsLimit: Int = Integer.MAX_VALUE): Dataset[Tag] = {
    import sparkSession.implicits._
    tagDataset.orderBy($"count".desc).limit(recordsLimit)
  }

  def toDataset(sparkSession: SparkSession, tagRDD: RDD[Tag]): Dataset[Tag] = {
    logger.info("Converting RDD to DataSet.")
    import sparkSession.implicits._
    sparkSession.createDataset(tagRDD)
  }

  def toDataFrame(sparkSession: SparkSession, tagRDD: RDD[Tag]): DataFrame = {
    logger.info("Converting RDD to DataFrame.")
    sparkSession.createDataFrame(tagRDD)
  }

}
