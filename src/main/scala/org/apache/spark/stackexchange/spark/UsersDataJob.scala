package org.apache.spark.stackexchange.spark

import java.util.Properties

import main.scala.org.apache.spark.stackexchange.data.parsing.UserParser
import org.apache.log4j.LogManager
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}
import org.apache.spark.stackexchange.commons.CaseImplicit.tryCase
import org.apache.spark.stackexchange.data._
import org.apache.spark.stackexchange.io.{FileSinkFromDataset, SinkFunction}
import org.apache.spark.stackexchange.spark.StackExchangeDataJob._

import scala.util.Try

object UsersDataJob {

  val logger = LogManager.getRootLogger
  val JOB_PREFIX = "users."

  def runPipeline(sparkSession: SparkSession, properties: Properties) = {
    logger.info("TagDataJob start")
    val recordsLimit = properties.getProperty(JOB_PREFIX + RECORDS_LIMIT_CONFIG_VALUE).toInt
    logger.info("Data read success. Start processing data")
    val inputData = getInputData(sparkSession, properties)
    val xmlRDD = parseInputXmlData(sparkSession, inputData)
    val userRDD = parseUserData(xmlRDD)
    val userDataset = toDataset(sparkSession, userRDD).cache
    logger.info(s"Data processed. Will now write $recordsLimit records to sink")
    toOutputFormat(sparkSession, userDataset, recordsLimit)
  }

  def getInputData(session: SparkSession, properties: Properties): Dataset[String] = {
    val pathToDataFile = properties.getProperty(JOB_PREFIX + DATA_FILE_CONFIG_VALUE)
    getInputDataFromFile(session: SparkSession, pathToDataFile)
  }

  def parseUserData(xmlRdd: RDD[Map[String, String]]): RDD[User] = {
    logger.info("Will now parse raw data to Users")
    xmlRdd.map(UserParser.xmlToTagData)
      .map(tryCase[User])
      .filter(_.isSuccess)
      .map(_.get)
  }

  def getSinkFunction(properties: Properties): SinkFunction[Dataset[UserResult]] = {
    new FileSinkFromDataset[UserResult](properties.getProperty(JOB_PREFIX + OUTPUT_FILE_CONFIG_VALUE))
  }

  def toOutputFormat(sparkSession: SparkSession, userDataset: Dataset[User], recordsLimit: Int = Integer.MAX_VALUE): Dataset[UserResult] = {
    val upVotesResults = mapToUserResult(
      sortAndLimit(sparkSession, userDataset, recordsLimit, "upVotes"),
      sparkSession,
      "upVotes"
    )
    val downVotesResults = mapToUserResult(
      sortAndLimit(sparkSession, userDataset, recordsLimit, "downVotes"),
      sparkSession,
      "downVotes"
    )
    upVotesResults.union(downVotesResults)
  }

  def sortAndLimit(sparkSession: SparkSession, userDataset: Dataset[User], recordsLimit: Int, dataField: String): Dataset[Row] = {
    import sparkSession.implicits._
    userDataset.groupBy($"location")
      .sum(dataField)
      .orderBy($"sum($dataField)".desc)
      .limit(recordsLimit)
  }

  def mapToUserResult(datasetSorted: Dataset[Row], sparkSession: SparkSession, dataField: String): Dataset[UserResult] = {
    import sparkSession.implicits._
    datasetSorted.map(
      result => UserResult(
        Try(result(0).toString).getOrElse(""),
        Try(result(1).toString.toInt).getOrElse(0),
        dataField)
    )
  }

  def toDataset(sparkSession: SparkSession, userRDD: RDD[User]): Dataset[User] = {
    import sparkSession.implicits._
    logger.info("Converting RDD to DataSet.")
    sparkSession.createDataset(userRDD)
  }

  def toDataFrame(sparkSession: SparkSession, userRDD: RDD[User]): DataFrame = {
    logger.info("Converting RDD to DataFrame.")
    sparkSession.createDataFrame(userRDD)
  }

}
