package main.scala.org.apache.spark.stackexchange.data.parsing

import org.apache.spark.stackexchange.commons.CaseImplicit.optionCase
import org.apache.spark.stackexchange.data.Tag

import scala.util.{Failure, Success, Try}

object TagParser {
  def xmlToTagData(fieldsMap: Map[String, String]): Try[Tag] = {
    try {
      val excerptPostId = optionCase(fieldsMap.get("ExcerptPostId")) match {
        case Success(value) => Some(value.toInt)
        case _ => None
      }
      val wikiPostId = optionCase(fieldsMap.get("WikiPostId")) match {
        case Success(value) => Some(value.toInt)
        case _ => None
      }
      val tag = Tag(
        fieldsMap("Id").toInt,
        fieldsMap("TagName"),
        fieldsMap("Count").toInt,
        excerptPostId,
        wikiPostId
      )
      Success(tag)
    } catch {
      case e: Exception => Failure(new IllegalArgumentException(s"Couldn't parse data: $fieldsMap, exception: $e"))
    }
  }
}
