package org.apache.spark.stackexchange.data

case class UserResult(location: String, votes: Int, votesType: String)
