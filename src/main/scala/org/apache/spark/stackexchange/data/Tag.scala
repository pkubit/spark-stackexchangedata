package org.apache.spark.stackexchange.data

case class Tag(id: Int, tagName: String, count: Int, excerptPostId: Option[Int] = None, wikiPostId: Option[Int] = None)
