package org.apache.spark.stackexchange.data

case class User(id: Int, location: Option[String], upVotes: Int, downVotes: Int)
