package main.scala.org.apache.spark.stackexchange.data.parsing

import org.apache.spark.stackexchange.commons.CaseImplicit._
import org.apache.spark.stackexchange.data.User

import scala.util.{Failure, Success, Try}

object UserParser {
  def xmlToTagData(fieldsMap: Map[String, String]): Try[User] = {
    try {
      val location = optionCase(fieldsMap.get("Location")) match {
        case Success(value) => Some(value)
        case _ => None
      }
      val user = User(
        fieldsMap("Id").toInt,
        location,
        fieldsMap("UpVotes").toInt,
        fieldsMap("DownVotes").toInt
      )
      Success(user)
    } catch {
      case e: Exception => Failure(new IllegalArgumentException(s"Couldn't parse data: $fieldsMap, exception: $e"))
    }
  }
}
