package org.apache.spark.stackexchange.commons


import org.apache.log4j.LogManager

import scala.util.{Failure, Success, Try}

object CaseImplicit {

  val logger = LogManager.getRootLogger

  def tryCase[T](input: Try[T]): Try[T] = {
    Try {
      input match {
        case Success(value) => logger.trace(s"[CaseImplicit] Success: $value")
          value
        case Failure(e) => logger.error(e)
          throw new IllegalArgumentException(e)
      }
    }
  }

  def optionCase[T](input: Option[T]): Try[T] = {
    Try (
    input match {
      case Some(value) => logger.trace(s"[CaseImplicit] Parsed Option: $value")
        value
      case None => logger.trace("Couldn't parse data")
        throw new IllegalArgumentException()
    }
    )
  }


}
