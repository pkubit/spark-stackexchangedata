package org.apache.spark.stackexchange.io

trait SinkFunction[A] {

  def createSink(input: A): Unit

}
