package org.apache.spark.stackexchange.io

import org.apache.spark.sql.Dataset

class FileSinkFromDataset[A](outputPath: String) extends SinkFunction[Dataset[A]] {

  override def createSink(input: Dataset[A]): Unit = {
    input.write.option("header", "true").mode("overwrite").csv(outputPath)
  }

}
