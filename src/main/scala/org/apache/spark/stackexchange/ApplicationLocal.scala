package org.apache.spark.stackexchange

import java.io.FileInputStream
import java.util.Properties

import org.apache.spark.sql.SparkSession
import org.apache.spark.stackexchange.Application.processData

import scala.util.Try


object ApplicationLocal {

  def main(args: Array[String]): Unit = {

    val configFilePath: String = Try(args(0)).getOrElse("src/main/resources/config.properties")
    val properties = loadProperties(configFilePath)

    val sparkSession = SparkSession.builder
      .appName("Simple Application")
      .config("spark.master", "local")
      .getOrCreate()

    processData(sparkSession, properties)

  }

  def loadProperties(configFilePath: String): Properties = {
    val configFile = new Properties()
    configFile.load(new FileInputStream(configFilePath))
    configFile
  }

}
